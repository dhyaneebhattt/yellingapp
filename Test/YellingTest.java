import static org.junit.Assert.*;

import org.junit.Test;

public class YellingTest
{

	@Test
	public void RC1() 
	{
		Yelling yell = new Yelling();
	    String a = yell.Scream("Peter");
	    assertEquals("Peter is yelling", a);
	}
	
	@Test
	public void RC2() 
	{
		Yelling yell = new Yelling();
	    String a = yell.Scream("");
	    assertEquals("Nobody is yelling", a);
	}
	
	@Test
	public void RC3() 
	{
		Yelling yell = new Yelling();
	    String a = yell.Scream("PETER");
	    assertEquals("PETER IS YELLING", a);
	}
	
	@Test
	public void RC4() 
	{
		Yelling yell = new Yelling();
	    String a = yell.Scream("Peter,Kadeem");
	    assertEquals("Peter and Kadeem are yelling", a);
	}
	
	@Test
	public void RC5() 
	{
		Yelling yell = new Yelling();
	    String a = yell.Scream("Peter,Kadeem,Albert");
	    assertEquals("Peter,Kadeem and Albert are yelling", a);
	}
	

}
